import styled from 'styled-components';

export const Container = styled.div`
  background: #fff;
  border-radius: 5px;
  border: 0;
  padding: 16px;
  width: 100%;
  height: 30px;
  color: #000;

  display: flex;
  align-items: center;

  input {
    color: #000;
    flex: 1;
    background: transparent;
    border: 0;
    padding: 10px;
  }

  & + & {
    margin-top: 18px;
  }
`;
