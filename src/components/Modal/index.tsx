import React, { useEffect, useState } from 'react';

import { FiX } from 'react-icons/fi';
import { FaBookReader } from 'react-icons/fa';
import { Container, Content, BookView } from './styles';
import api from '../../services/api';

interface ModalInterface {
  onClose: () => void;
  id?: string;
}

interface BookInfo {
  volumeInfo: {
    title: string;
    authors: [];
    publisher: string;
    publishedDate: string;
    description: string;
    imageLinks: {
      thumbnail: string;
    };
  };
  accessInfo: {
    webReaderLink: string;
  };
}

const Modal: React.FC<ModalInterface> = ({
  id = 'outside',
  onClose,
  children,
}) => {
  const [book, setBook] = useState<BookInfo>();

  useEffect(() => {
    api.get(`volumes/${children}`).then((response) => {
      setBook(response.data);
    });
  }, [children]);

  const handleOutsideClick = (e: any): void => {
    if (e.target.id === id) onClose();
  };

  return (
    <Container id={id} onClick={(e) => handleOutsideClick(e)}>
      <Content>
        <FiX color="white" size={22} onClick={onClose} />
        {book && (
          <>
            <BookView>
              <div className="Img">
                <img
                  src={book.volumeInfo.imageLinks.thumbnail}
                  alt={book.volumeInfo.title}
                />
              </div>
              <div className="Info">
                <strong>{book.volumeInfo.title}</strong>
                <div className="authors">
                  <div className="name">
                    <span>Autores:</span>
                    {book.volumeInfo.authors.map((author) => (
                      <p key={author}>{author}</p>
                    ))}
                  </div>
                  <div className="read">
                    <a
                      rel="noopener noreferrer"
                      target="_blank"
                      href={book.accessInfo.webReaderLink}
                    >
                      <FaBookReader color="white" size={20} />
                      <span>Ler</span>
                    </a>
                  </div>
                </div>
                <div className="published">
                  <div className="who">
                    <span>Publicado por:</span>
                    <p>{book.volumeInfo.publisher}</p>
                  </div>
                  <div className="when">
                    <span>Em</span>
                    <p>{book.volumeInfo.publishedDate}</p>
                  </div>
                </div>
                <div className="description">
                  <p>
                    <span>Descrição: </span>
                    {book.volumeInfo.description}
                  </p>
                </div>
              </div>
            </BookView>
          </>
        )}
      </Content>
    </Container>
  );
};

export default Modal;
