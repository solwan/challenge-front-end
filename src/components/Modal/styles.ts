import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 260vh;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
  background-color: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;

  @media only screen and (max-width: 1180px) {
    margin-top: -200vh;
    height: 420vh;
  }
  @media only screen and (max-width: 614px) {
    margin-top: -200vh;
    height: 550vh;
  }
`;

export const Content = styled.div`
  background-color: #000;
  width: 70%;
  height: auto;
  border-radius: 20px;

  strong {
    font-size: 20px;
  }

  svg {
    right: calc(-100% + 35px);
    position: relative;
    top: 8px;
    cursor: pointer;
  }

  @media only screen and (max-width: 768px) {
    width: 90%;
  }
`;

export const BookView = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  height: auto;
  max-height: 800px;

  .Img {
    width: 27%;
    margin: 10px 10px;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      border-color: #fff;
      border: 2px solid;
      width: 190px;
      height: 300px;
    }
  }

  .Info {
    width: 70%;
    display: flex;
    flex-direction: column;

    strong,
    span,
    p {
      color: #fff;
    }

    strong {
      width: 100%;
      max-width: 660px;
      height: 34px;
      font-style: normal;
      font-weight: bold;
      font-size: 25px;
      line-height: 34px;
      color: #fcdb00;
    }

    span {
      color: #fcdb00;
      width: 12%;
      max-width: 791px;
      height: 31px;
      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 18px;
      text-align: center;
    }

    .authors {
      margin-top: 16px;
      display: flex;
      width: 70%;
      justify-content: space-around;
      align-items: center;

      .name {
        display: flex;
        align-items: center;
        flex-direction: row;
        width: 76%;
      }

      a {
        text-decoration: none;
        display: flex;
        align-items: center;
        flex-direction: row-reverse;
        width: 38%;
        justify-content: space-around;
      }

      p {
        margin-left: 16px;
        margin-top: 13px;
        margin-right: 16px;
      }
    }

    .published {
      margin-top: 16px;
      display: flex;
      flex-direction: row;
      align-items: center;
      width: 86%;

      .who {
        width: 50%;
        display: flex;
        flex-direction: row;
        align-items: center;
        span {
          width: 40%;
        }
      }

      .when {
        width: 50%;
        display: flex;
        flex-direction: row;
        align-items: center;
        span {
          width: 20%;
        }
      }

      p {
        margin-bottom: -16px;
      }
    }

    .description {
      width: 90%;
      max-width: 580px;
      margin-left: 18px;
      margin-top: 38px;
      margin-bottom: 40px;

      p {
        span {
          margin-right: 16px;
        }
      }
    }
  }

  @media only screen and (max-width: 1180px) {
    margin-top: 100px;
    margin-bottom: 100px;
  }

  @media only screen and (max-width: 1124px) {
    height: auto;
    max-height: 2000px;
    .Img {
      margin-bottom: 20px;
      img {
        height: 190px;
      }
    }
    .Info {
      margin-top: 20px;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;

      strong {
        width: 100%;
        max-width: 260px;
        margin-left: 36px;
        margin-bottom: 40px;
      }

      .authors {
        margin-top: 40px;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-around;
        flex-wrap: wrap;
        span {
          margin-right: 16px;
          margin-left: 16px;
        }
      }

      .published {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-around;
        flex-wrap: wrap;
        span {
          margin-right: 16px;
          margin-left: 16px;
        }
      }

      .description {
        margin-right: 20px;
        width: 90%;
        height: 100%;
        max-height: 1200px;
        max-width: 240px;
        margin-left: 18px;
        margin-top: 38px;
        margin-bottom: 40px;
      }
    }
  }
`;
