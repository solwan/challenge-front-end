import React from 'react';

import { Switch, Route } from 'react-router-dom';

import Listagem from '../pages/listagem/index';

import Detalhe from '../pages/detalhe/index';

import Home from '../pages/home/index';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Home} />

    <Route path="/detalhe" component={Detalhe} />
    <Route path="/listagem" component={Listagem} />
  </Switch>
);

export default Routes;
