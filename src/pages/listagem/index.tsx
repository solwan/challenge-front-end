import React from 'react';

import { Link } from 'react-router-dom';
import { Container } from './styles';

import Input from '../../components/Input/index';

const Listagem: React.FC = () => {
  return (
    <>
      <Container>
        <div className="top">
          <div className="menu">
            <Link to="/">
              <h1>Books</h1>
            </Link>
            <Link to="/detalhe">
              <h1>Newsletter</h1>
            </Link>
            <Link to="/listagem">
              <h1>Address</h1>
            </Link>
          </div>
        </div>
        <div className="content">
          <div className="yellowBox">
            <strong>Filter</strong>
            <form>
              <Input placeholder="palavra-passe" />
              <Input placeholder="titulo" />
              <Input placeholder="categoria" />
              <Input placeholder="ano" />
              <Input placeholder="author" />
              <Input placeholder="categoria" />
              <button type="button">
                <p>SUBMIT</p>
              </button>
            </form>
          </div>
        </div>
        <div className="buttonFooter">
          <button type="button">
            <p>LOAD MORE</p>
          </button>
        </div>
      </Container>
    </>
  );
};

export default Listagem;
