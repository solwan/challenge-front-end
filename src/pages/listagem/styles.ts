import Styled from 'styled-components';

export const Container = Styled.div`
  .top {
    background-color: #FCDB00;
    width: 100%;
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: center;
    height: 123px;

    .menu {
      width: 30%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      margin-right: 83px;
      flex-wrap: wrap;
      height: auto;

      a {
        text-decoration: none;
      }

      h1 {
        cursor: pointer;

        & + & {
          margin: 0px 10px;
        }
      }
    }
  }

  .content{
    width: 100%;
    display: flex;
    margin-left: 83px;

    .yellowBox{
      margin-top: 70px;
      width: 100%;
      max-width: 300px;
      height: 443px;
      background: #FCDB00;

      display: flex;

      flex-direction: column;
      text-align: left;

      strong {
        width: 66px;
        height: 34px;
        margin-top: 31px;
        margin-left: 30px;
        margin-bottom: 20px;
        font-style: normal;
        font-weight: bold;
        font-size: 25px;
        line-height: 34px;
        color: #FFFFFF;
      }

      form {
        width: 240px;
        margin-left: 30px;

        button {
          position: absolute;
          width: 240px;
          height: 30px;
          background: #010101;
          border-radius: 5px;
          border: 0px;
          padding: 5px 66px;
          margin-top: 20px;

          p {
            width: 111.11px;
            height: 18px;
            font-style: normal;
            font-weight: bold;
            font-size: 13px;
            line-height: 18px;
            text-align: center;
            color: #FBFBFB;
          }
        }
      }
    }
  }

  .buttonFooter{
    margin-top: 41px;
    width: 70%;
    display: flex;
    flex-direction: row-reverse;

    button {
      width: 300px;
      height: 40px;
      background: #FCDB00;
      border-radius: 5px;
      border: 0px;
      padding: 5px 66px;

      p {
        font-weight: bold;
      }
    }
  }

  @media only screen and (max-width: 860px) {
    .top {
      display: flex;
      align-items: center;
      justify-content: center;
      height: auto;
      .menu {
        a {
          margin-top: 7px;
          margin-bottom: 7px;
        }
      }
    }
    .content{
      display: flex;
      align-items: center;
      justify-content: center;
      margin-left: 0px;

    }
    .buttonFooter{
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: row;
    }
  }
`;
