import Styled from 'styled-components';

export const Container = Styled.div`
  width: 100%;
  /* height: 100vh; */
  display: flex;
  align-items: center;
  flex-direction: column;

`;

export const Header = Styled.div`
  background-color: #FCDB00;
  width: 100%;
  height: 100vh;

  display: flex;
  align-items: center;
  flex-direction: column;

  .top {
    width: 80%;
    height: auto;
    flex-wrap: wrap;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 70px;

    .logo {
      width: 300px;
      img {
        width: 149px;
        height: 43px;
      }
    }

    .menu {
      width: 30%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      flex-wrap: wrap;
      height: auto;

      a {
        text-decoration: none;
      }

      h1 {
        cursor: pointer;

        & + & {
          margin: 0px 10px;
        }
      }
    }
  }


  .body {
    width: 85%;
    height: auto;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 106px;
    flex-wrap: wrap;

    > img {
      width: 100%;
      max-width: 316px;
      min-width: 300px;
      height: 470px;
    }

    .info {
      width: 100%;
      max-width: 684px;

      h1 {
        margin-top: 69px;
        margin-bottom: 36px;
      }

      h3 {
        width: 100%;
        max-width: 321px;
        height: 39px;
        font-style: normal;
        font-weight: normal;
        font-size: 17px;
        line-height: 23px;
        color: #000000;
        margin-bottom: 36px;
      }

      p {
        width: 100%;
        max-width: 684px;
        height: 74px;
        font-style: normal;
        font-weight: 300;
        font-size: 17px;
        line-height: 23px;
        color: #000000;
        margin-bottom: 38px;
      }

      .icons{
        width: 25%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }
    }
  }

  .points {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 8%;
    min-width: 120px;


    .point {
      width: 20px;
      height: 20px;
      border-radius: 50%;

      background: #FFFFFF;
    }

    .point1 {
      width: 20px;
      height: 20px;
      border-radius: 50%;

      background: rgba(255, 255, 255, 0.7);
    }
  }

  @media only screen and (max-width: 1180px) {
    position: relative;
    height: auto;

    .points {
      margin-top: 50px;
      margin-bottom: 30px;
    }
  }

  @media only screen and (max-width: 550px) {
    .body {
      .info {
        margin-top: -40%;
      }

      >img {
        margin-top: 40px;
        margin-left: -25px;
      }

      .icons{
        margin-top: 150px;
        min-width: 272px;

      }
    }

  }

`;

export const Content = Styled.div`
  background-color: #FFF;
  width: 100%;
  height: 100vh;


  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  > strong{
    margin-top: 80px;

    font-style: normal;
    font-weight: bold;
    font-size: 25px;
    line-height: 34px;
  }

  span {
    width: 100%;
    max-width: 791px;
    height: 31px;
    margin-top: 28px;
    color: #555555;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 18px;
    text-align: center;
  }

  .grid_books {
    width: 40%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;

    margin-top: 35px;
    height: auto;
  }

  @media only screen and (max-width: 1180px) {
    position: relative;
    height: auto;
  }

`;

export const Footer = Styled.div`
  background-color: #000;
  width: 100%;
  height: 584px;

  display: flex;
  align-items: center;
  flex-direction: column;

  strong {
    margin-top: 80px;
    width: 270px;
    height: 34px;
    font-style: normal;
    font-weight: bold;
    font-size: 25px;
    line-height: 34px;
    color: #FCDB00;
  }

  span {
    width: 100%;
    max-width: 791px;
    margin-top: 28px;
    height: 31px;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 18px;
    text-align: center;
  }

  span, p {
    color: #F1F1F1;
  }

  form {
      margin-top: 50px;
      width: 100%;
      max-width: 578px;
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;

    .input{
      width: 450px;

      input {
        height: 10px;
        border: 0;
        background: #FFFFFF;
        border-radius: 5px;
      }
    }

    button {
      width: 108px;
      height: 30px;
      border: 0;
      background: #FCDB00;
      border-radius: 5px;
      padding: 7px 30px;

      p {
        width: 50px;
        height: 18px;

        font-style: normal;
        font-weight: bold;
        font-size: 13px;
        line-height: 18px;
        color: #333333;
      }
    }
  }

  .icons{
    display: flex;
    align-items: center;
    justify-content: space-around;
    flex-direction: row;
    width: 100%;
    max-width: 330px;
    margin-top: 50px;
  }

  .places {
    display: flex;
    align-items: center;
    justify-content: space-around;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;
    margin-top: 63px;

    .SP{
      width: 154px;
      height: 91px;

      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 100%;
      display: flex;
      align-items: center;
    }

    .UK {
      width: 117px;
      height: 65px;

      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 100%;
      display: flex;
      align-items: center;
    }

    .LB {
      width: 150px;
      height: 91px;

      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 100%;
      display: flex;
      align-items: center;
    }

    .PR{
      width: 142px;
      height: 65px;

      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 100%;
      display: flex;
      align-items: center;
    }

    .BA{
      width: 152px;
      height: 65px;

      font-style: normal;
      font-weight: normal;
      font-size: 13px;
      line-height: 100%;
      display: flex;
      align-items: center;
    }
  }

  @media only screen and (max-width: 740px) {
    height: auto;
  }

  @media only screen and (max-width: 572px) {
    form {
      display: flex;
      justify-content: center;
      align-items: center;
      button {
        margin-top: 20px;
      }
    }
  }

`;

export const Button = Styled.button`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  align-items: center;
  flex: 1;
  background: transparent;
  border: 0;
  transition: background-color 0.2s;
  margin: 10px;



  img {
    width: 100px;
    height: 130px;
    margin-bottom: 10px;
    margin-top: 10px;
  }


`;
