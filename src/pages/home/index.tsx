import React, { useState, useEffect } from 'react';

import {
  FaFacebookSquare,
  FaGooglePlusSquare,
  FaPinterestSquare,
} from 'react-icons/fa';

import { AiFillTwitterSquare } from 'react-icons/ai';

import { Link } from 'react-router-dom';
import { Container, Button, Content, Header, Footer } from './styles';

import Input from '../../components/Input';

import Logo from '../../assets/logo.png';
import HeaderBook from '../../assets/header-book.png';
import Apple from '../../assets/icons/apple.png';
import Windows from '../../assets/icons/windows.png';
import Android from '../../assets/icons/android.png';

import api from '../../services/api';

import Modal from '../../components/Modal/index';

interface Books {
  selfLink: string;
  volumeInfo: {
    id: number;
    title: string;
    imageLinks: {
      thumbnail: string;
    };
  };
}

const Home: React.FC = () => {
  const [books, SetBooks] = useState<Books[]>([]);
  const [showModal, setShowModal] = useState(false);
  const [bookLink, setBookLink] = useState('');

  const [email, setEmail] = useState('');

  useEffect(() => {
    api.get('volumes?q=HARRY%20POTTER').then((response) => {
      SetBooks(response.data.items);
    });
  }, []);

  function sendPayload(e: any): void {
    e.preventDefault();
    alert(`Site: Pixter Books, email: ${email}.`);
    setEmail('');
  }

  function openModal(link: string): void {
    const [, , , , , , setimo] = link.split('/');
    setShowModal(true);
    setBookLink(setimo);
  }

  return (
    <>
      <Container>
        <Header>
          <div className="top">
            <div className="logo">
              <img src={Logo} alt="logo" />
            </div>
            <div className="menu">
              <Link to="/">
                <h1>Books</h1>
              </Link>
              <Link to="/detalhe">
                <h1>Newsletter</h1>
              </Link>
              <Link to="/listagem">
                <h1>Address</h1>
              </Link>
            </div>
          </div>

          <div className="body">
            <div className="info">
              <h1>Pixter Digital Books</h1>
              <h3>Lorem ipsum dolor sit amet? consectetur elit, volutpat</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Praesent vitae eros eget tellus tristique bibendum. Donec rutrum
                sed sem quis venenatis. Proin viverra risus a eros volutpat
                tempor. In quis arcu et eros porta lobortis sit
              </p>
              <div className="icons">
                <img src={Apple} alt="" />
                <img src={Windows} alt="" />
                <img src={Android} alt="" />
              </div>
            </div>
            <img src={HeaderBook} alt="" />
          </div>
          <div className="points">
            <div className="point" />
            <div className="point1" />
            <div className="point1" />
          </div>
        </Header>

        <Content>
          {showModal && (
            <Modal onClose={() => setShowModal(false)}>{bookLink}</Modal>
          )}
          <strong>Books</strong>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
            venenatis.
          </span>
          <div className="grid_books">
            {books.map((book) => (
              <Button
                key={book.selfLink}
                type="button"
                onClick={() => openModal(book.selfLink)}
              >
                <img
                  src={book.volumeInfo.imageLinks.thumbnail}
                  alt={book.volumeInfo.title}
                />
              </Button>
            ))}
          </div>
        </Content>

        <Footer>
          <strong>Keep in touch with us</strong>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
            vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
            venenatis.
          </span>
          <form onSubmit={sendPayload}>
            <div className="input">
              <Input
                type="email"
                placeholder="enter your email to update"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <button type="submit">
              <p>SUBMIT</p>
            </button>
          </form>

          <div className="icons">
            <FaFacebookSquare color="#FCDB00" size={50} />
            <AiFillTwitterSquare color="#FCDB00" size={54} />
            <FaGooglePlusSquare color="#FCDB00" size={50} />
            <FaPinterestSquare color="#FCDB00" size={50} />
          </div>
          <div className="places">
            <div className="SP">
              <p>
                Alameda Santos, 1970 6th floor - Jardim Paulista São Paulo - SP
                +55 11 3090 8500
              </p>
            </div>
            <div className="UK">
              <p>London - UK 125 Kingsway London WC2B 6NH</p>
            </div>
            <div className="LB">
              <p>
                Lisbon - Portugal Rua Rodrigues Faria, 103 4th floor Lisbon -
                Portugal
              </p>
            </div>
            <div className="PR">
              <p>Curitiba – PR R. Francisco Rocha, 198 Batel – Curitiba – PR</p>
            </div>
            <div className="BA">
              <p>Buenos Aires – Argentina Esmeralda 950 Buenos Aires B C1007</p>
            </div>
          </div>
        </Footer>
      </Container>
    </>
  );
};

export default Home;
