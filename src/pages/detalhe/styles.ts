import Styled from 'styled-components';

export const Container = Styled.div`
  .top {
    background-color: #FCDB00;
    width: 100%;
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: center;
    height: 123px;

    .menu {
      width: 30%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      margin-right: 83px;
      flex-wrap: wrap;
      height: auto;

      a {
        text-decoration: none;
      }

      h1 {
        cursor: pointer;

        & + & {
          margin: 0px 10px;
        }
      }
    }
  }

  .content{
    width: 50%;
    display: flex;
    margin-left: 83px;
    align-items: center;
    justify-content: space-around;
    flex-wrap: wrap;

    .session {
      margin-top: 70px;

      .page {
        width: 300px;
        height: 422px;

        background: #FFFFFF;
        border: 1px solid #D0D0D0;
        box-sizing: border-box;
      }

      .icons {
        margin-top: 14.25px;
        display: flex;
        align-items: center;
        justify-content: space-around;
        width: 100%;
        max-width: 150px;
        margin-left: 50%;
      }
    }


    .text {
      width: 100%;
      max-width: 460px;

      strong {
        width: 263px;
        height: 26px;
        font-style: normal;
        font-weight: bold;
        font-size: 19px;
        line-height: 26px;
        color: #000000;
      }

      .lorem {
        margin-top: 22px;

        span {
          width: 460px;
          height: 275px;

          font-style: normal;
          font-weight: normal;
          font-size: 12px;
          line-height: 185.5%;
          letter-spacing: 0.01em;

          color: #262626;
        }
      }

      .buttons{
        display: flex;
        align-items: center;
        justify-content: space-around;
        flex-direction: row;
        width: 300px;
        margin-top: 59px;

        .buy {
          button {
            width: 124px;
            height: 40px;
            border: 0;
            background: #FCDB00;
            border-radius: 5px;
            padding: 10px 48px;

            p {
              width: 27px;
              height: 18px;
              font-style: normal;
              font-weight: bold;
              font-size: 13px;
              line-height: 18px;
              color: #333333;
            }
          }
        }

        .wish {
          button {
            width: 124px;
            height: 40px;
            border: 0;
            background: #FB0505;
            border-radius: 5px;
            padding: 0px 30px;

            p {
              width: 64px;
              height: 18px;

              font-style: normal;
              font-weight: bold;
              font-size: 13px;
              line-height: 18px;
              color: #FFFFFF;
            }
          }
        }
      }
    }
  }

  .buttonFooter{
    margin-top: 41px;
    width: 70%;
    display: flex;
    flex-direction: row-reverse;

    button {
      width: 300px;
      height: 40px;
      background: #FCDB00;
      border-radius: 5px;
      border: 0px;
      padding: 5px 66px;

      p {
        font-weight: bold;
      }
    }
  }

  @media only screen and (max-width: 860px) {
    .top {
      display: flex;
      align-items: center;
      justify-content: center;
      height: auto;
      .menu {
        a {
          margin-top: 7px;
          margin-bottom: 7px;
        }
      }
    }
  }

  @media only screen and (max-width: 1534px) {
    .content{
      .text{
        margin-top: 30px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;

        .lorem {
          margin-top: 30px;
        }

        .buttons{
          margin-bottom: 20px;
        }
      }
    }
  }

`;
