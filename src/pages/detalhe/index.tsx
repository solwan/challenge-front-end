import React from 'react';

import {
  FaFacebookSquare,
  FaGooglePlusSquare,
  FaPinterestSquare,
} from 'react-icons/fa';

import { AiFillTwitterSquare } from 'react-icons/ai';

import { Link } from 'react-router-dom';
import { Container } from './styles';

const Detalhe: React.FC = () => {
  return (
    <>
      <Container>
        <div className="top">
          <div className="menu">
            <Link to="/">
              <h1>Books</h1>
            </Link>
            <Link to="/detalhe">
              <h1>Newsletter</h1>
            </Link>
            <Link to="/listagem">
              <h1>Address</h1>
            </Link>
          </div>
        </div>
        <div className="content">
          <div className="session">
            <div className="page" />
            <div className="icons">
              <FaFacebookSquare color="#FCDB00" size={20} />
              <AiFillTwitterSquare color="#FCDB00" size={20} />
              <FaGooglePlusSquare color="#FCDB00" size={20} />
              <FaPinterestSquare color="#FCDB00" size={20} />
            </div>
          </div>
          <div className="text">
            <strong>Lorem ipsum dolor sit amet</strong>
            <div className="lorem">
              <span>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Integer condimentum tellus sed congue vestibulum. Pellentesque
                  interdum id lectus ac accumsan. Suspendisse sodales, turpis ac
                  sodales finibus, urna nibh aliquet lacus, vel bibendum enim
                  neque et mauris. Nunc felis lorem, pharetra ac fringilla
                  porttitor, lobortis et nunc. Mauris auctor scelerisque ligula,
                  at tristique arcu consequat quis. Aenean eget condimentum leo,
                  ut egestas tellus.
                </p>
                <br />
                <p>
                  Curabitur ligula libero, dictum sed felis in, tempus ornare
                  erat. Nam venenatis neque at ex aliquet, eu convallis ligula
                  lacinia. Pellentesque quis sodales nibh, in iaculis ligula.
                  Etiam tincidunt sed quam convallis auctor. Maecenas venenatis,
                  lacus eget ultrices consequat, elit urna convallis nunc, vel
                  placerat lacus tortor id tortor.
                </p>
              </span>
            </div>
            <div className="buttons">
              <div className="buy">
                <button type="button">
                  <p>BUY</p>
                </button>
              </div>
              <div className="wish">
                <button type="button">
                  <p>WISH LIST</p>
                </button>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Detalhe;
